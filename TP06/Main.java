import java.util.Scanner;
public class Main {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int jumlah = scan.nextInt();
        Karyawan [] listKaryawan = new Karyawan[jumlah];
        for (int i = 0; i < jumlah;i++){
            String nama = scan.next();
            int umur = scan.nextInt();
            int lamaBekerja = scan.nextInt();
            Karyawan objectKaryawan = new Karyawan(nama, umur, lamaBekerja);
            listKaryawan[i] = objectKaryawan;
        }
        System.out.printf("Rata-rata gaji karyawan adalah %.2f%n",rerataGaji(listKaryawan));
        System.out.println("Karyawan dengan gaji tertinggi adalah " + gajiTertinggi(listKaryawan));
        System.out.println("Karyawan dengan gaji terendah adalah " + gajiTerendah(listKaryawan));

    }

    //TODO
    public static double rerataGaji(Karyawan[] listKaryawan) {
        double totalGaji = 0;
        for (Karyawan obj : listKaryawan){
            totalGaji += obj.getGaji();
        }
        return totalGaji/listKaryawan.length;
    }
    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTerendah(Karyawan[] listKaryawan) {
        Karyawan min = listKaryawan[0];
        for (int i = 1; i < listKaryawan.length; i++){
            Karyawan acuan = listKaryawan[i];
            if (acuan.getGaji() < min.getGaji()){
                min = acuan;
            }
        }
        return min.getNama();
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTertinggi(Karyawan[] listKaryawan) {
        Karyawan max = listKaryawan[0];
        for (int i = 1; i < listKaryawan.length; i++){
            Karyawan acuan = listKaryawan[i];
            if (acuan.getGaji() > max.getGaji()){
                max = acuan;
            }
        }
        return max.getNama();
    }

}
