public class Mahasiswa {
    private String nama;
    private String npm;
    private String jurusan;

    public Mahasiswa(String nama, String npm, String jurusan) {
        this.nama = nama;
        this.npm = npm;
        this.jurusan = jurusan;
    }

    public Mahasiswa() {
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }
}
