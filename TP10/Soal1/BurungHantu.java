package Soal1;
import Soal2.BisaTerbang;

public class BurungHantu extends Hewan implements BisaTerbang {

    public BurungHantu(String nama, String spesies) {
        super(nama,spesies);
    }

    @Override
    public String bernafas() {
            return (this.getNama() + " bernafas dengan paru - paru.");
    }

    @Override
    public String bersuara() {
        return ("Hooooh hoooooooh. (Halo, saya" + this.getNama() + ". Saya adalah burung hantu).");
    }

    @Override
    public String bergerak() {
        return (this.getNama() + " bergerak dengan cara terbang.");
    }

    @Override
    public String terbang() {
        return ("Whooooooshh saya terbang!");
    }

}
