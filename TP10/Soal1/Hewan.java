package Soal1;

public abstract class Hewan implements Makhluk  {
    private String nama;
    private String spesies;

    protected Hewan(String nama, String spesies) {
        this.nama = nama;
        this.spesies = spesies;
    }

    public String bersuara() {
        return null;
    }

    public String bernafas() {
        return null;
    }

    public String bergerak() {
        return null;
    }

    public String getNama() {
        return nama;
    }

    public String getSpesies() {
        return spesies;
    }

    @Override
    public String toString() {
        return this.bersuara();
    }
}
