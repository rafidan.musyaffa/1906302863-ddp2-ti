package Soal1;

public class Ikan extends Hewan {
    private boolean isBeracun;

    public Ikan(String nama, String spesies, boolean isBeracun) {
        super(nama,spesies);
        this.isBeracun = isBeracun;
    }

    @Override
    public String bernafas() {
        return (this.getNama() + " bernafas dengan insang.");
    }

    @Override
    public String bersuara() {
        if (this.isBeracun == true) {
            return ("Blub blub blub blub. Blub. (Halo, saya" + this.getNama() + ". Saya adalah ikan yang beracun).");
        }else{
            return ("Blub blub blub blub. Blub. (Halo, saya" + this.getNama() + ". Saya adalah ikan yang tidak beracun).");
        }
    }

    @Override
    public String bergerak() {
        return (this.getNama() + " bergerak dengan cara berenang.");
    }

    public boolean getIsBeracun() {
        return isBeracun;
    }
}
