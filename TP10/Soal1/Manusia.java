package Soal1;

public abstract class Manusia implements Makhluk {
    private String nama;
    private int uang;

    public Manusia(String nama, int uang) {
        this.nama = nama;
        this.uang = uang;
    }

    public String bicara() {
        return ("Halo, saya " + this.getNama() + ". Uang saya adalah " + this.getUang() + ".");
    }

    public String bernafas() {
        return (this.getNama() + " bernafas dengan paru-paru.");
    }

    public String bergerak() {
        return (this.getNama() + " bergerak dengan cara berjalan.");
    }

    public String getNama() {
        return nama;
    }

    public int getUang() {
        return uang;
    }

    @Override
    public String toString() {
        return this.bicara();
    }
}
