package Soal1;

public class Pegawai extends Manusia{
    private String levelKeahlian;

    public Pegawai(String nama, int uang, String levelKeahlian){
        super(nama,uang);
        this.levelKeahlian = levelKeahlian;
    }

    public String getLevelKeahlian() {
        return levelKeahlian;
    }

    public String bekerja() {
        return (this.getNama() + " bekerja di kedai VoidMain.");
    }

    @Override
    public String bicara() {
        return ("Halo, saya " + this.getNama() + ". Uang saya adalah " + this.getUang() + 
        ", dan level keahlian saya adalah " + this.getLevelKeahlian() + ".");
    }

}
