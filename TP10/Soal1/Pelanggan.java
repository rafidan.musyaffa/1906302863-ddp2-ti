package Soal1;

public class Pelanggan extends Manusia {

    public Pelanggan(String nama, Integer uang){
        super(nama,uang);
    }

    public String Membeli() {
        return (this.getNama() + " membeli makanan dan minuman di kedai VoidMain.");
    }
}
