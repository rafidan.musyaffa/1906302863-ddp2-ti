package Soal1;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

import Soal2.IkanSpesial;
import Soal2.PegawaiSpesial;

public class Simulator {
    private static Makhluk cari(ArrayList<Makhluk> arraym, String find ){
        for (int i = 0;i< arraym.size();i++){
            if (arraym.get(i).getNama().equalsIgnoreCase(find)){
                return arraym.get(i);
            }
        }
        return null;
    }
    public static void main(String[] args){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000 , "Master");
        Manusia bujang = new Pelanggan("Bujang", 100000);
        Hewan kerapu = new Ikan("Kerapubatik", "Epinephelus Polyphekadion", false);
        Hewan biru = new IkanSpesial("ikanterbangbiru", "Exocoetus volitans", true);
        Hewan burhan = new BurungHantu("Burhan", "Bubo Scandiacus");

        ArrayList <Makhluk> m = new ArrayList<>();
        m.add(yoga);
        m.add(bujang);
        m.add(kerapu);
        m.add(biru);
        m.add(burhan);

        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Silahkan Masukan perintah: ");
            String[] arrPerintah = input.nextLine().toLowerCase().split(" ");
            if (arrPerintah[0].equals("selesai")){
                break;
            }else {
                try{
                Makhluk cariMakhluk = cari(m,arrPerintah[0]);
                Makhluk cariMakhluk2 = cari(m,arrPerintah[1]);
                    if (cariMakhluk == null && cariMakhluk2 == null){
                        if (arrPerintah[0].equals("panggil")){
                            System.out.println("Maaf, " + arrPerintah[1] + " tidak pernah melewati/mampir di kedai.");
                        }else if (arrPerintah[1].equals("serang")){
                            System.out.println("Maaf, tidak ada makhluk bernama " + arrPerintah[0] + ".");
                        }else{
                            System.out.println("Maaf, tidak ada makhluk bernama " + arrPerintah[0] + ".");
                        }
                    }else if (arrPerintah[0].equals("panggil")){
                        System.out.println(cariMakhluk2.toString());
                    }else if (arrPerintah[1].equals("bergerak")){
                        System.out.println(cariMakhluk.bergerak());    
                    }else if (arrPerintah[1].equals("bernafas")){
                        System.out.println(cariMakhluk.bernafas());                          
                    }else if (arrPerintah[0].equals("ikanterbangbiru") && arrPerintah[1].equals("terbang")){
                        try{
                            IkanSpesial temp = (IkanSpesial) cariMakhluk;
                            System.out.println(temp.terbang());
                        }catch (Exception e){
                            System.out.println(arrPerintah[0] + " tidak bisa terbang.");
                        }
                    }else if (arrPerintah[0].equals("burhan") && arrPerintah[1].equals("terbang")){
                        try{
                            BurungHantu temp = (BurungHantu) cariMakhluk;
                            System.out.println(temp.terbang());
                        }catch (Exception e){
                            System.out.println(arrPerintah[0] + " tidak bisa terbang.");
                        }
                    }else if (arrPerintah[1].equals("bekerja")){
                        try{
                            Pegawai temp = (Pegawai) cariMakhluk;
                            System.out.println(temp.bekerja());
                        }catch (Exception e){
                            System.out.println(arrPerintah[0] + " tidak bisa bekerja.");
                        }                         
                    }else if (arrPerintah[1].equals("bersuara")){
                        Hewan temp = (Hewan) cariMakhluk;
                        System.out.println(temp.bersuara());                       
                    }else if (arrPerintah[1].equals("libur")){
                        try{
                            PegawaiSpesial temp = (PegawaiSpesial) cariMakhluk;
                            System.out.println(temp.Libur());
                        }catch(Exception e){
                            System.out.println("Maaf pegawai " + arrPerintah[0] + " tidak bisa libur.");
                        }                       
                    }else if (arrPerintah[1].equals("beli")){
                        try{
                            Pelanggan temp = (Pelanggan) cariMakhluk;
                            System.out.println(temp.Membeli());
                        }catch(Exception e){
                            System.out.println("Maaf, " + arrPerintah[0] + " tidak bisa beli.");
                        }                       
                    }else{
                        if(arrPerintah[1].equals("serang")){
                            System.out.println("Maaf, Pegawai " + arrPerintah[0] + " tidak bisa serang.");
                        }else{
                            System.out.println("Maaf, perintah tersebut tidak valid.");
                        }
                    }
                }catch(ArrayIndexOutOfBoundsException e){
                    System.out.println("Maaf, perintah tidak ditemukan");
                }
               
            }

        } 
        System.out.println("Sampai jumpa!");
        input.close();
    }
}
