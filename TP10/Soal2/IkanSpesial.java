package Soal2;
import Soal1.*;

public class IkanSpesial extends Ikan implements BisaTerbang {
    private boolean isBeracun;

    public IkanSpesial(String nama, String spesies, boolean isBeracun) {
        super(nama,spesies,isBeracun);
    }

    public String terbang() {
        return ("Fwooosssshhhhh! Plup.");
    }

    @Override
    public String bersuara() {
        if (this.isBeracun == true) {
            return ("Blub blub blub blub. Blub. (Halo, saya " + this.getNama() + ". Saya adalah ikan yang beracun. Saya bisa terbang loh.).).");
        }else{
            return ("Blub blub blub blub. Blub. (Halo, saya " + this.getNama() + ". Saya adalah ikan yang tidak beracun. Saya bisa terbang loh.).");
        }
    }

}

