package Soal2;
import Soal1.*;

public class PegawaiSpesial extends Pegawai implements BisaLibur {

    public PegawaiSpesial(String nama, int uang, String levelKeahlian){
        super(nama,uang,levelKeahlian);
    }

    @Override
    public String bergerak() {
        return ("Halo, saya " + this.getNama() + ". Uang saya adalah " + this.getUang() + 
        ", dan level keahlian saya adalah " + this.getLevelKeahlian() + ". Saya memiliki privilage yaitu bisa libur.");
    }

    public String Libur() {
        return (this.getNama() + " sedang berlibur ke Akihabara");
    }
}
