package Soal4;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import Soal1.*;

public class TestTP10_Soal1 {
    @Test
    public void testManusia() {
        Manusia coba = new Pelanggan("Rafi", 100000);
        assertEquals("Rafi", coba.getNama());
        assertEquals(100000, coba.getUang());
    }

    @Test
    public void testPegawai() {
        Pegawai coba = new Pegawai("Dafiq", 100000, "pemula");
        assertEquals("Dafiq", coba.getNama());
        assertEquals(100000, coba.getUang());
        assertEquals("pemula", coba.getLevelKeahlian());
    }

    @Test
    public void testPelanggan() {
        Pelanggan coba = new Pelanggan("Rafi", 100000);
        assertEquals("Rafi", coba.getNama());
        assertEquals(100000, coba.getUang());
    }

    @Test
    public void testIkan() {
        Ikan coba = new Ikan("Betta","Betta Splendens", true);
        assertEquals("Betta", coba.getNama());
        assertEquals("Betta Splendens", coba.getSpesies());
        assertEquals(true, coba.getIsBeracun());
    }

    @Test
    public void testBurungHantu() {
        BurungHantu coba = new BurungHantu("Burhan", "Bubo Scandiacus");
        assertEquals("Burhan", coba.getNama());
        assertEquals("Bubo Scandiacus", coba.getSpesies());
    }

    @Test
    public void testHewan() {
        Hewan coba = new BurungHantu("Joni", "Joni Splendens");
        assertEquals("Joni", coba.getNama());
        assertEquals("Joni Splendens", coba.getSpesies());
    }

}
