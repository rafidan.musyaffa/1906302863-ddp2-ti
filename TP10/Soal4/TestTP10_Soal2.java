package Soal4;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import Soal2.*;

public class TestTP10_Soal2 {
    @Test
    public void testIkanSpesial() {
        IkanSpesial coba = new IkanSpesial("kerapubatik","Epinephelus Polyphekadion", true);
        assertEquals("kerapubatik", coba.getNama());
        assertEquals("Epinephelus Polyphekadion", coba.getSpesies());
        assertEquals(true, coba.getIsBeracun());
    }

    @Test
    public void testPegawaiSpesial() {
        PegawaiSpesial coba = new PegawaiSpesial("Rafi",100000, "master");
        assertEquals("Rafi", coba.getNama());
        assertEquals(100000, coba.getUang());
        assertEquals("master", coba.getLevelKeahlian());
    }
}
