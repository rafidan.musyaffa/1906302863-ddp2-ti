public class Elektronik extends Barang {
    String jenis;
    String kondisi;

    public Elektronik(String jenis, String kondisi) {
        this.kondisi = kondisi;
        this.jenis = jenis;
    }

    @Override
    public double getValue(){
        double valueBarang = 0.0;
        double valueKondisi = 0.0;
        if(getJenis().equals("laptop")){
            valueBarang = 500.0;
        }else if(getJenis().equals("hp")){
            valueBarang = 200.0;
        }else if(getJenis().equals("modem")){
            valueBarang = 100.0;
        }
        if(getKondisi().equals("buruk")){
            valueKondisi = 0.25;
        }else if(getKondisi().equals("menengah")){
            valueKondisi = 0.8;
        }else if(getKondisi().equals("baik")){
            valueKondisi = 1.0;
        }else if(getKondisi().equals("baru")){
            valueKondisi = 1.25;
        }
        return valueBarang*valueKondisi;
    }

    public String toString(){
        return ("sesuatu");
    }

    //method getter
    public String getJenis(){
        return  jenis;
    }

    public String getKondisi(){
        return  kondisi;
    }
}
