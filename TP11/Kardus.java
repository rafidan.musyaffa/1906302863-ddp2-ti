import java.util.*;

public class Kardus<T extends Barang>{
    private List<T> listBarang = new ArrayList<T>();

    public String rekap(){
        int counterE = 0;
        int counterP = 0;
        for (T item : this.listBarang){
            try{
                Pakaian p = (Pakaian) item;
                counterP += 1;
            }catch(Exception e){
                counterE += 1;
            }
        }
        String temp = "";
        if(counterE == 0){
            temp = "Pakaian";
        }else if(counterP == 0){
            temp = "Elektronik";
        }else{
            temp = "Campuran";
        }
        return "Kardus " + temp + " : Terdapat " + counterE + " barang elektronik dan " + counterP + " pakaian ";
    }

    public void tambahBarang(T item){
        this.listBarang.add(item);
    }

    public double getTotalValue(){
        double totalValue = 0.0;
        for (T item : this.listBarang){
            totalValue += item.getValue();
        }
        return totalValue;
    }
}
