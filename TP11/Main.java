import java.util.Scanner;

public class Main {
    public static void main(String[] args){

        System.out.println("Selamat datang di layanan donasi Desa Dedepe Dua!");
        System.out.println("Layanan disponsori oleh Kedai VoidMain");

        Scanner input = new Scanner(System.in);
        System.out.println("Ada berapa jumlah barang untuk elektronik, pakaian, dan campuran?");
        String[] arrPerintah = input.nextLine().split(" ");
        
        
        Kardus<Elektronik> kardusElektronik = new Kardus<Elektronik>();
        System.out.println("Silahkan masukan keterangan barang elektronik");
        System.out.println("Dengan format ' jenis kondisi ' tanpa tanda ' ");
        for (int i = 0; i < Integer.parseInt(arrPerintah[0]); i++) {
            String[] arrElektronik = input.nextLine().split(" ");
            Elektronik tempElektronik = new Elektronik(arrElektronik[0], arrElektronik[1]);
            kardusElektronik.tambahBarang(tempElektronik); 
        }
        System.out.println(kardusElektronik.getTotalValue());

        Kardus<Pakaian> kardusPakaian = new Kardus<Pakaian>();
        System.out.println("Silahkan masukan keterangan barang pakaian");
        System.out.println("Dengan format ' ukuran warna ' tanpa tanda ' ");
        for (int i = 0; i < Integer.parseInt(arrPerintah[1]); i++) {
            String[] arrPakaian = input.nextLine().split(" ");
            Pakaian tempPakaian = new Pakaian(arrPakaian[0].charAt(0), arrPakaian[1]);
            kardusPakaian.tambahBarang(tempPakaian);
        }
        System.out.println(kardusPakaian.getTotalValue());

        Kardus<Barang> kardusCampuran = new Kardus<Barang>();
        System.out.println("Silahkan masukan keterangan barang campuran");
        System.out.println("Dengan format ' ELEKTRONIK kondisi ' tanpa tanda ' atau ' PAKAIAN ukuran warna ' untuk pakaian");
        for (int i = 0; i < Integer.parseInt(arrPerintah[2]); i++) {
            String[] arrBarang = input.nextLine().split(" ");
            if (arrBarang[0].equalsIgnoreCase("elektronik")) {
                Barang tempCampuran = new Elektronik(arrBarang[1], arrBarang[2]);
                kardusCampuran.tambahBarang(tempCampuran);
            } else if (arrBarang[0].equalsIgnoreCase("pakaian")) {
                Barang tempCampuran = new Pakaian(arrBarang[1].charAt(0), arrBarang[2]);
                kardusCampuran.tambahBarang(tempCampuran);
            } else {
                System.out.println("Jenis barang tidak ditemukan!");
            }
        }
        System.out.println(kardusCampuran.getTotalValue());
        double totalValue = kardusElektronik.getTotalValue() + kardusPakaian.getTotalValue()
                + kardusCampuran.getTotalValue();

        System.out.println("-----------------------------------");
        System.out.println("Terima kasih atas donasi anda.");
        System.out.println("Donasi anda sebesar " + totalValue + " DDD");
        System.out.println("Rekap donasi untuk tiap kardus:");
        System.out.println(kardusElektronik.rekap());
        System.out.println(kardusPakaian.rekap());
        System.out.println(kardusCampuran.rekap());
        System.out.println("-----------------------------------");

    }
}
