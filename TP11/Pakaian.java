public class Pakaian extends Barang {
    char ukuran;
    String warna;

    public Pakaian(char ukuran, String warna) {
        this.ukuran = ukuran;
        this.warna = warna;
    }

    @Override
    public double getValue(){
        double valueUkuran = 0.0;
        if(getUkuran() =='l'){
            valueUkuran = 40.0;
        }else if(getUkuran() =='m'){
            valueUkuran = 35.0;
        }else if(getUkuran() =='s'){
            valueUkuran = 30.0;
        }
        return valueUkuran;
    }

    public String toString(){
        return ("sesuatu");
    }

    //method getter
    public char getUkuran(){
        return  ukuran;
    }

    public String getWarna(){
        return warna;
    }
}
