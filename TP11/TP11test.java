import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TP11test {

    @Test
    public void testPakaian() {
        Pakaian coba = new Pakaian('l', "merah");
        assertEquals('l', coba.getUkuran());
        assertEquals("merah", coba.getWarna());
    }

    @Test
    public void testElektronik() {
        Elektronik coba = new Elektronik("laptop", "baru");
        assertEquals("laptop", coba.getJenis());
        assertEquals("baru", coba.getKondisi());
    }

    @Test
    public void testValuePakaian() {
        Pakaian cobaL = new Pakaian('l', "merah");
        Pakaian cobaM = new Pakaian('m', "merah");
        Pakaian cobaS = new Pakaian('s', "merah");
        assertEquals(40.0, cobaL.getValue(), 0);
        assertEquals(35.0, cobaM.getValue(), 0);
        assertEquals(30.0, cobaS.getValue(), 0);
    }

    @Test
    public void testValueElektronik() {
        Elektronik coba1 = new Elektronik("laptop", "baru");
        Elektronik coba2 = new Elektronik("hp", "menengah");
        Elektronik coba3 = new Elektronik("modem", "baik");
        Elektronik coba4 = new Elektronik("laptop", "buruk");
        assertEquals(625.0, coba1.getValue(),0);
        assertEquals(160.0, coba2.getValue(),0);
        assertEquals(100.0, coba3.getValue(),0);
        assertEquals(125.0, coba4.getValue(),0);
    }
}
