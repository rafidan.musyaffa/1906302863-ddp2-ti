public class Dosen extends Manusia {
    private String nip;
    private MataKuliah[] daftarMatkul;

    public Dosen(String nama,String nip){
        super(nama);
        this.nip = nip;
        this.daftarMatkul = new MataKuliah[7];
    }

    public void assignMatkul(MataKuliah matkul){
        for (int i= 0; i<daftarMatkul.length;i++){
            if(daftarMatkul[i] == null){
                daftarMatkul[i] = matkul;
            }
        }
    }

    public String getNip(){
        return nip;
    }

    @Override
    public String toString(){
        return (getNip() + ", " + getNama());
    }

    public String seluruhMatkul(){
        String matkulS = "";
        for(int i = 0; i < daftarMatkul.length; i++){
            if(daftarMatkul[i] != null){
                matkulS += daftarMatkul[i].getNama() + "\n";
            }
        }
        return matkulS;
    }

    public MataKuliah[] getDaftarMatkul(){
        return  daftarMatkul;
    }
}
