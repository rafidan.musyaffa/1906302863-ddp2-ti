import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.io.File;
import java.io.FileNotFoundException;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;

public class DosenTest {
    
    @Test
    public void testConstructorDosen(){
        Dosen dsn1 = new Dosen("Bapak Bayu","199457389");
        assertEquals(dsn1.getNama(), "Bapak Bayu");
        assertEquals(dsn1.getNip(), "199457389");
        assertEquals(dsn1.toString(), "199457389, Bapak Bayu");
    }

    @Test
    public void testDosen01(){
        Dosen dsn1 = new Dosen("Ade Azurat","129500004Y");
        MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");

        dsn1.assignMatkul(kul1);

        assertEquals(kul1.getKategori(), "Mata Kuliah Wajib Fakultas");
 
        MataKuliah[] daftarMatkul = dsn1.getDaftarMatkul();
        List<MataKuliah> arr = Arrays.asList(daftarMatkul);
        
        //mengecek apakah terdapat object kul 1 dan 2 pada list
        assertTrue(arr.contains(kul1));
    }
}
