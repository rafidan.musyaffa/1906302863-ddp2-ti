public class Mahasiswa extends Manusia{
    private MataKuliah[] daftarMatkul;
    private String npm;

    public Mahasiswa(String nama, String npm) {
        super(nama);
        this.npm = npm;
        this.daftarMatkul = new MataKuliah[7];
    }

    public void tambahMatkul(MataKuliah matkul){
        int i = 0;
        try{
            while(i<daftarMatkul.length){
                i ++;
                if (daftarMatkul[i] == null){
                    daftarMatkul[i] = matkul;
                    break;
                }
            }
        }catch (Exception e){
            System.out.println("Mata kuliah sudah ada.");;
        }
        
    }

    public void dropMatkul(MataKuliah matkul){
        int i = 0;
        try{
            while(i<daftarMatkul.length){
                i ++;
                if (daftarMatkul[i].equals(matkul)){
                    daftarMatkul[i] = null;
                    break;
                }
            }
        }catch (Exception e){
            System.out.println("Mata kuliah tidak ada.");
        }
        
    }

    //geter untuk npm
    public String getNpm(){
        return  npm;
    }

    @Override
    public String toString(){
        return this.getNpm() + ", " + this.getNama();
    }

    //geter untuk daftar matkul
    public MataKuliah[] getDaftarMatkul(){
        return  daftarMatkul;
    }

    public String seluruhMatkul(){
        String matkulS = "";
        for(int i = 0; i < daftarMatkul.length; i++){
            if(daftarMatkul[i] != null){
                matkulS += (i) + ". " + daftarMatkul[i].getNama() + " : " + daftarMatkul[i].getKategori() + "\n";
            }
        }
        return matkulS;
    }

}
