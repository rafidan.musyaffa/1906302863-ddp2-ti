import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.io.File;
import java.io.FileNotFoundException;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
 
public class MahasiswaTest {

    @Test
    public void testConstructorMahasiswa(){
        Mahasiswa mhs1 = new Mahasiswa("Rafi","199457389");
        assertEquals(mhs1.getNama(), "Rafi");
        assertEquals(mhs1.getNpm(), "199457389");
        assertEquals(mhs1.toString(), "199457389, Rafi");
    }

    @Test
    public void testKuliah02(){
        Mahasiswa mhs1 = new Mahasiswa("Burhan","129500004Y");
        MataKuliah kul2 = new MataKuliah("PPW","CSGO123456");
        MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");

        mhs1.tambahMatkul(kul1);
        mhs1.tambahMatkul(kul2);
        System.out.println(mhs1.seluruhMatkul());


        assertEquals(kul1.getKategori(), "Mata Kuliah Wajib Fakultas");
 
        MataKuliah[] daftarMatkul = mhs1.getDaftarMatkul();
        List<MataKuliah> arr = Arrays.asList(daftarMatkul);
        
        //mengecek apakah terdapat object kul 1 dan 2 pada list
        assertTrue(arr.contains(kul1));
        assertTrue(arr.contains(kul2));

        mhs1.dropMatkul(kul2);

        //memastikan bahwa method drop matkul berhasil
        assertFalse(arr.contains(kul2));

    }

}

