import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.BufferedWriter;

public class MataKuliah {
    private Mahasiswa[] daftarMhs;
    private Dosen[] daftarDosen;
    private String kode;
    private String nama;

    public MataKuliah(String nama, String kode) {
        this.daftarMhs = new Mahasiswa[7];
        this.daftarDosen = new Dosen[7];
        this.kode = kode;
        this.nama = nama;
    }

    public void tambahMhs(Manusia mhs){
        int i = 0;
        try{
            while(i<daftarMhs.length){
                i ++;
                if (daftarMhs[i] == null){
                    daftarMhs[i] =(Mahasiswa) mhs;
                    break;
                }
            }
        }catch (Exception e){
            System.out.println("Mahasiswa sudah ada.");
        }
        
    }

    public void assignDosen(Manusia dosen){
        int i = 0;
        try{
            while(i<daftarDosen.length){
                i++;
                if (daftarDosen[i] == null){
                    daftarDosen[i] = (Dosen) dosen;
                    break;
                }
            }
        }catch (Exception e){
            System.out.println("Dosen sudah ada.");
        }
        
    }

    public void dropMhs(Manusia mhs){
        int i = 0;
        try{
            while(i<daftarMhs.length){
                i ++;
                if (daftarMhs[i].equals(mhs)){
                    daftarMhs[i] = null;
                    break;
                }
            }
        }catch (Exception e){
            System.out.println("Mahasiswa tidak ada.");
        }
        
    }

    //getter untuk kode
    public String getKode(){
        return  kode;
    }

    public String getKategori(){
        String temp ;
        if(getKode().toUpperCase().startsWith("UIGE")){
            temp = "Mata Kuliah Wajib Universitas";
        }else if(getKode().toUpperCase().startsWith("UIST")){
            temp = "Mata Kuliah Wajib Rumpun Sains dan Teknologi";
        }else if(getKode().toUpperCase().startsWith("CSGE")){
            temp = "Mata Kuliah Wajib Fakultas";
        }else if(getKode().toUpperCase().startsWith("CSCM")){
            temp = "Mata Kuliah Wajib Program Studi Ilmu Komputer";
        }else if(getKode().toUpperCase().startsWith("CSIM")){
            temp = "Mata Kuliah Wajib Program Studi Sistem Informasi";
        }else if(getKode().toUpperCase().startsWith("CSCE")){
            temp = "Mata Kuliah Peminatan Program Studi Ilmu Komputer";
        }else if(getKode().toUpperCase().startsWith("CSIE")){
            temp = "Mata Kuliah Peminatan Program Studi Sistem Informasi";
        }else{
            temp = "Mata kuliah tidak memiliki kategori.";
        }
        return temp;
    }

    //geter untuk nama
    public String getNama(){
        return  nama;
    }

    public String toString(){
        return (this.getKode() + ", " + this.getNama());
    }

    //geter untuk daftar mahasiswa
    public Mahasiswa[] getDaftarMhs(){
        return  daftarMhs;
    }

    public Dosen[] getDaftarDosen(){
        return  daftarDosen;
    }

    public ArrayList<String> assignData (Mahasiswa[] mhs, Dosen[] dosen){
        ArrayList<String> mainData = new ArrayList<String>();
        mainData.add(getKode() + ", " + getNama());
        int i = 0;
        while (i<dosen.length){
            if (dosen[i] != null){
                mainData.add(dosen[i].toString());
            }
            i++;
        }
        int j = 0;
        while (j<mhs.length){
            if (mhs[j] != null){
                mainData.add(mhs[j].toString());
            }
            j++;
        }
        return mainData;
    }

    public String printMatkulData(ArrayList<String> mainData){
        String fixData = "";
        for (int i=0;i<mainData.size();i++){
            fixData += mainData.get(i) + "\n";
        }
        return fixData;
    }

    public void writeToCSV (String isi){
        try {
            FileWriter writer = new FileWriter("Matkul.csv");
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(isi);
            bw.close();
        } catch (IOException e){
            e.printStackTrace();
        }   
    }

    public ArrayList<String[]> ReadFile(String file) {
        ArrayList<String[]> mainFile = new ArrayList<String[]>();
        String[] data;
        try {
            File obj = new File(file);
            Scanner sc = new Scanner(obj);
            while(sc.hasNextLine()) {
                data = sc.nextLine().split(", ");    
                mainFile.add(data);
            }
            sc.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
        return  mainFile;
    }

    public String cetakCSV (ArrayList<String[]> tes){
        String hasil = "";
        for (int i = 0; i<tes.size();i++){
            String[] x = tes.get(i);
            for (int j = 0; j < x.length; j++){
                hasil += (x[j] + " ");
            }
            hasil += "\n";
        }
        return hasil;
    }
    
}
