import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.io.File;
import java.io.FileNotFoundException;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class MataKuliahTest {
    @Test
    public void testConstructorMatakuliah(){
        MataKuliah kul1 = new MataKuliah("PPW","CSGE601021");
        assertEquals(kul1.getNama(), "PPW");
        assertEquals(kul1.getKode(), "CSGE601021");
        assertEquals(kul1.getKategori(), "Mata Kuliah Wajib Fakultas");
        assertEquals(kul1.toString(), "CSGE601021, PPW");
    }
    
    @Test
    public void testKuliah01(){
        Mahasiswa mhs1 = new Mahasiswa("Burhan","129500004Y");
        Mahasiswa mhs2 = new Mahasiswa("Bubur","1299123456");
        MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");
        Mahasiswa mhs3 = new Mahasiswa("Bulan","1299167456");
        Dosen dsn1 = new Dosen("Ade Azurat","129500004Y");

        kul1.assignDosen(dsn1);
        kul1.tambahMhs(mhs1);
        kul1.tambahMhs(mhs2);
        
 
        Mahasiswa[] daftarMhs = kul1.getDaftarMhs();
        Dosen[] daftarDosen = kul1.getDaftarDosen();
        List<Mahasiswa> arr = Arrays.asList(daftarMhs);
        List<Dosen> arr2 = Arrays.asList(daftarDosen);
 
        //mengecek apakah terdapat object mhs 1 dan 2 pada list
        assertTrue(arr.contains(mhs1));
        assertTrue(arr.contains(mhs2));
        assertTrue(arr2.contains(dsn1));
 
        kul1.dropMhs(mhs1);
        kul1.dropMhs(mhs3);
        kul1.getKategori();

        //memastikan bahwa method drop mahasiswa berhasil 
        assertFalse(arr.contains(mhs1));

        ArrayList<String> mainData = kul1.assignData(daftarMhs,daftarDosen);
        System.out.println(kul1.printMatkulData(mainData));
        String hasil = kul1.printMatkulData(mainData);
        kul1.writeToCSV(hasil);
        ArrayList<String[]> temp = kul1.ReadFile("Matkul.csv");
        String test = kul1.cetakCSV(temp);
        System.out.println(test);
        
        // simpen ke suatu variabel trus variabelnya panggil writefile parameternya si variabel
    }
    
}
