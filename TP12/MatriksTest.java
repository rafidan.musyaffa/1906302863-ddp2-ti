import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.io.File;
import java.io.FileNotFoundException;
import org.junit.Test;

public class MatriksTest {
    
    @Test
    public void testcekDiagonal(){
        Integer[][] m1 = new Integer[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 4, 5, 6 } };
        Integer[][] m2 = new Integer[][] { { 1, 1, 1 }, { 2, 2, 1 }, { 4, 5, 6 } };
        Integer[][] m3 = new Integer[][] { { 1, 1 }, { 2, 2 }, { 4, 5 } };

        Rational[][] r1 = new Rational[3][3];
        Rational[][] r2 = new Rational[3][3];
        for (int i = 0; i < r1.length; i++)
            for (int j = 0; j < r1[0].length; j++) {
                r1[i][j] = new Rational(i + 1, j + 5);
                r2[i][j] = new Rational(i + 1, j + 6);
            }
        assertEquals(GenericMatrix.cekDimensi(m1,m2), "Matriks memiliki dimensi yang sama");
        assertEquals(GenericMatrix.cekDimensi(m1,m3), "Matriks memiliki dimensi yang berbeda");
        assertEquals(GenericMatrix.cekDimensi(r1,r2), "Matriks memiliki dimensi yang sama");
    }

    //bukan test tetapi untuk mencoba apakah benar atau tidak outputnya
    @Test
    public void testcekBusang(){
        Integer[][] m1 = new Integer[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 4, 5, 6 } };
        Integer[][] m2 = new Integer[][] { { 2, 2, 1 }, { 4, 5, 6 } };

        GenericMatrix.cekBusang(m1);
        GenericMatrix.cekBusang(m2);
    }

    
    //bukan test tetapi untuk mencoba apakah benar atau tidak outputnya
    @Test
    public void testDiagonalPrimer(){
        Integer[][] m1 = new Integer[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 4, 5, 6 } };
        
        GenericMatrix.diagonalPrimer(m1);
    }

    
    //bukan test tetapi untuk mencoba apakah benar atau tidak outputnya
    @Test
    public void testSegitigaAtas(){
        Integer[][] m1 = new Integer[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 4, 5, 6 } };

        GenericMatrix.segitigaAtas(m1);
    }

}
