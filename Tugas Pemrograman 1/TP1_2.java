import java.util.Scanner;

public class TP1_2 {
    public static void main(String[] args){
        System.out.println("Selamat Datang di DDP2");
        Scanner inp = new Scanner(System.in);
        System.out.print("Nama Fakultas apa yang anda ingin ketahui? ");
        String fakultas=inp.next().toUpperCase();
        
        switch (fakultas) {
            case "FASILKOM":
                System.out.println("Fakultas Ilmu Komputer");
                break;
            case "FPSI":
                System.out.println("Fakultas Psikologi");
                break;
            case "FIK":
                System.out.println("Fakultas Ilmu Keperawatan");
                break;
            case "FH":
                System.out.println("Fakultas Hukum");
                break;
            case "FKG":
                System.out.println("Fakultas Kedokteran Gigi");
                break;
            case "FK":
                System.out.println("Fakultas Kedokteran");
                break;
            case "FKM":
                System.out.println("Fakultas Kesehatan Msyarakat");
                break;
            case "FT":
                System.out.println("Fakultas Teknik");
                break;
            case "FIA":
                System.out.println("Fakultas Ilmu Adiministrasi");
                break;
            case "FIB":
                System.out.println("Fakultas Ilmu Pengetahuan Budaya");
                break;
            case "FF":
                System.out.println("Fakultas Farmasi");
                break;
            case "FEB":
                System.out.println("Fakultas Ekonomi dan Bisnis");
                break;
            case "FISIP":
                System.out.println("Fakultas Ilmu Sosial dan Politik");
                break;
            case "FMIPA":
                System.out.println("Fakultas Matematika dan Ilmu Pengetahuan Alam");
                break;
            default:
                System.out.println("Fakultas tidak ditemukan");
        }
    }
}