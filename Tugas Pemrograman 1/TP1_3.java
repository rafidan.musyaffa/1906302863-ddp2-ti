import java.util.Scanner;

public class TP1_3{
    public static void main(String[] args){

        System.out.println("Selamat datang di Bank Dedepedua!");
        Scanner inp = new Scanner(System.in);
        System.out.print("Apa tipe kartu anda? ");
        String tipe = inp.next().toUpperCase();
        System.out.print("Sejak tahun berapa anda menjadi member? ");
        int tahun = inp.nextInt();
        double hargaBarang = 200000;
        int persen = 0;

        switch (tipe){
            case "GOLD":
                persen = 25;
                break;
            case "SILVER":
                persen = 15;
                break;
            case "BRONZE":
                persen = 10;
                break;
            default :
                persen = 0;
                break;
        }
        
        if (tahun <= 2017){
            persen += 5;
        }
        double diskon = hargaBarang * persen/100;
        double hargaAkhir = hargaBarang - diskon;
        System.out.println("Dapat diskon sebanyak "+ persen + " persen");
        System.out.println("Harga barang menjadi " + "Rp" + hargaAkhir);
    }
}