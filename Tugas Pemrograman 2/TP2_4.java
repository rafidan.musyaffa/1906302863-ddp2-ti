import java.util.Scanner;

public class TP2_4 {
    public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        int ab = inp.nextInt();
        int bc = inp.nextInt();
        int ac = inp.nextInt();
        int temp = 0;
        String pemimpin = "";

        if (ab > bc && ab > ac){
            pemimpin = "c";
            temp = ac;
            ac = ab;
            ab = temp; 
        } else if (bc > ab && bc > ac){
            pemimpin = "a";
            temp = ac;
            ac = bc;
            ab = temp;
        } else if (ac > bc && ac > ab){
            pemimpin = "b";
        }

        if (Math.pow(ab,2)+Math.pow(bc,2)==Math.pow (ac,2)){
            System.out.println("Segitiga siku - siku, mereka penyusup");
            System.out.println(pemimpin + " pemimpin mereka");
        }else{
            System.out.println("Bukan segitiga siku - siku, mereka bukan penyusup");
        }
    }
}  